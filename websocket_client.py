import sys
import atexit
import time
import queue
import threading
import dhttp
import trio


host = sys.argv[1]

if len(sys.argv) > 3:
    port = int(sys.argv[2])
    path = sys.argv[3]

else:
    port = 80
    path = sys.argv[2]


print("ws://{}:{}/{}".format(host, port, path.lstrip('/')))

read_lines = queue.Queue()
ws = dhttp.DHTTPWebsocketClient(host, port)
running = True

@atexit.register
def stop_running():
    running = False


def read_loop():
    while running:
        read_lines.put(sys.stdin.readline().strip('\r\n'))
        time.sleep(0.1)

threading.Thread(target=read_loop, name='stdin read loop').start()

try:
    @ws.on_connect
    async def _on_connected(client, resp):
        print('--- Connected!')
        print()

        buffer = [b'']

        @client.ws_receiver
        async def got_data(pkt):
            print(pkt)
            buffer[0] += pkt.content
            lines = buffer[0].split(b'\n')
            buffer[0] = lines[-1]
            lines = [x.strip('\r') for x in lines[:-1]]

            for l in lines:
                print('<<< ', repr(l))

        @client.on_close
        async def got_everything():
            running = False

        while running:
            read_lines.put(None)

            for l in iter(read_lines.get_nowait, None):
                print('>>> ', repr(l))
                packet = client.ws_write(l + '\r\n')
                print(packet.encode().hex())

            await trio.sleep(1 / 30)

    async def run_things():
        await ws.connect('/echo', tls = True, cert_file = '.client.cert.pem', key_file = '.client.key.pem')
        await ws.run_loop()
    
    try:
        trio.run(run_things)

    except KeyboardInterrupt:
        stop_running()

except BaseException as err:
    running = False
    raise
